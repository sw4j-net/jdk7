# Introduction

This project creates a docker image with (open)jdk 7 installed.

The image can be used as a base image for other images or to run applications needing java.

This repository is mirrored to https://gitlab.com/sw4j-net/jdk7

## Deprecation

As Java 7 is deprecated this repository and the generated images will be removed in January 2019.
